// Copyright 2023 Grigory Kirilin
//
// Licensed under the Apache NON-AI License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://github.com/non-ai-licenses/non-ai-licenses/blob/main/NON-AI-APACHE2
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "test_catch.hpp"
#include "epath/epath.hpp"
#include "epath/original.hpp"

TEST_CASE("One can construct escaped path", "[epath][escape_path]")
{
  auto str = std::string{};
  auto ref = std::string{};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"%"};
  ref = std::string{"%25"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"%25"};
  ref = std::string{"%2525"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"%%%"};
  ref = std::string{"%25%25%25"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"d%slk%aks"};
  ref = std::string{"d%25slk%25aks"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"dre%slkas%"};
  ref = std::string{"dre%25slkas%25"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"dreslkas"};
  ref = std::string{"dreslkas"};

  REQUIRE(escape_path(str) == ref);

  str = std::string{"%dreslkas"};
  ref = std::string{"%25dreslkas"};

  REQUIRE(escape_path(str) == ref);
}

TEST_CASE("Reference implementation of escaped path", "[epath][find_string_escape]")
{
  auto str = std::string{};
  auto ref = std::string{};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"%"};
  ref = std::string{"%25"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"%25"};
  ref = std::string{"%2525"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"%%%"};
  ref = std::string{"%25%25%25"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"d%slk%aks"};
  ref = std::string{"d%25slk%25aks"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"dre%slkas%"};
  ref = std::string{"dre%25slkas%25"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"dreslkas"};
  ref = std::string{"dreslkas"};

  REQUIRE(find_string_escape(str) == ref);

  str = std::string{"%dreslkas"};
  ref = std::string{"%25dreslkas"};

  REQUIRE(find_string_escape(str) == ref);
}

TEST_CASE("One can construct escaped path with regex (pre-allocated)",
          "[epath][escape_path_regex_prealloc]")
{
  auto str = std::string{};
  auto ref = std::string{};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"%"};
  ref = std::string{"%25"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"%25"};
  ref = std::string{"%2525"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"%%%"};
  ref = std::string{"%25%25%25"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"d%slk%aks"};
  ref = std::string{"d%25slk%25aks"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"dre%slkas%"};
  ref = std::string{"dre%25slkas%25"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"dreslkas"};
  ref = std::string{"dreslkas"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);

  str = std::string{"%dreslkas"};
  ref = std::string{"%25dreslkas"};

  REQUIRE(escape_path_regex_prealloc(str) == ref);
}

TEST_CASE("One can construct escaped path with regex", "[epath][escape_path_regex]")
{
  auto str = std::string{};
  auto ref = std::string{};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"%"};
  ref = std::string{"%25"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"%25"};
  ref = std::string{"%2525"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"%%%"};
  ref = std::string{"%25%25%25"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"d%slk%aks"};
  ref = std::string{"d%25slk%25aks"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"dre%slkas%"};
  ref = std::string{"dre%25slkas%25"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"dreslkas"};
  ref = std::string{"dreslkas"};

  REQUIRE(escape_path_regex(str) == ref);

  str = std::string{"%dreslkas"};
  ref = std::string{"%25dreslkas"};

  REQUIRE(escape_path_regex(str) == ref);
}

TEST_CASE("One can construct escaped path with boost", "[epath][escape_path_boost]")
{
  auto str = std::string{};
  auto ref = std::string{};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"%"};
  ref = std::string{"%25"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"%25"};
  ref = std::string{"%2525"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"%%%"};
  ref = std::string{"%25%25%25"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"d%slk%aks"};
  ref = std::string{"d%25slk%25aks"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"dre%slkas%"};
  ref = std::string{"dre%25slkas%25"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"dreslkas"};
  ref = std::string{"dreslkas"};

  REQUIRE(escape_path_boost(str) == ref);

  str = std::string{"%dreslkas"};
  ref = std::string{"%25dreslkas"};

  REQUIRE(escape_path_boost(str) == ref);
}
