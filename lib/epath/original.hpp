#pragma once
#include <string>
#include <string_view>

/**
 *
 * @details Original function from the [blogpost](https://lemire.me/blog/2023/10/19/for-processing-strings-streams-in-c-can-be-slow/).
 *
 */
inline std::string find_string_escape(std::string_view file_path)
{
  // Avoid unnecessary allocations.
  size_t pos = file_path.empty() ? std::string_view::npos : file_path.find('%');
  if (pos == std::string_view::npos) {
    return std::string(file_path);
  }
  // Escape '%' characters to a temporary string.
  std::string escaped_file_path;

  do {
    escaped_file_path += file_path.substr(0, pos + 1);
    escaped_file_path += "25";
    file_path          = file_path.substr(pos + 1);
    pos = file_path.empty() ? std::string_view::npos : file_path.find('%');
  } while (pos != std::string_view::npos);
  escaped_file_path += file_path;
  return escaped_file_path;
}
