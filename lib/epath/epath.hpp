// Copyright 2023 Grigory Kirilin
//
// Licensed under the Apache NON-AI License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://github.com/non-ai-licenses/non-ai-licenses/blob/main/NON-AI-APACHE2
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#pragma once
#include <algorithm>
#include <string>
#include <string_view>
#include <regex>

#include "boost/algorithm/string/replace.hpp"

/**
 *
 * @details Here no string specific standard algorithms were used,
 * you can replace standard string by any container with a random access.
 *
 */
inline std::string escape_path(std::string_view path)
{
  auto const count = std::count(path.cbegin(), path.cend(), '%');
  if (!count) return std::string(path.cbegin(), path.cend());

  auto result = std::string(path.size() + 2 * count, '%');
  auto alpha  = path.cbegin();
  auto omega  = std::find(alpha, path.cend(), '%');
  auto target = std::copy(alpha, omega, result.begin());

  do {
    target[1] = '2';
    target[2] = '5';
    alpha     = omega + 1;
    omega     = std::find(alpha, path.cend(), '%');
    target    = std::copy(alpha, omega, target + 3);
  } while(target != result.end());
  return result;
}

/**
 * @details Binding arguments of the standard regex replace algorithm.
 */
inline std::string escape_path_regex(std::string const &path)
{
  static auto const re = std::regex{"%"};
  return std::regex_replace(path, re, "$&25");
}

/**
 * @details Binding arguments of the standard regex replace algorithm.
 * This version uses pre-allocated memory for the result.
 */
inline std::string escape_path_regex_prealloc(std::string const &path)
{
  auto const count = std::count(path.cbegin(), path.cend(), '%');
  if (!count) return path;

  auto result = std::string(path.size() + 2 * count, ' ');
  static auto const re = std::regex{"%"};
  std::regex_replace(result.begin(), path.cbegin(), path.cend(), re, "$&25");
  return result;
}

/**
 * @details Binding arguments of the Boost's replace algorithm.
 */
inline std::string escape_path_boost(std::string path)
{
  boost::algorithm::replace_all(path, "%", "%25");
  return path;
}
