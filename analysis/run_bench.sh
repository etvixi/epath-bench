#!/bin/bash

build/bench/bin/bench --benchmark_format=csv --benchmark_enable_random_interleaving=true | tee epath-bench/analysis/config_02.csv
sleep 32

for (( i = 0; i < 10; i++ )); do
    build/bench/bin/bench --benchmark_format=csv --benchmark_enable_random_interleaving=true | tail --lines=+2 | tee -a epath-bench/analysis/config_02.csv
    sleep 32
done

clang-build/bench/bin/bench --benchmark_format=csv --benchmark_enable_random_interleaving=true | tee epath-bench/analysis/config_03.csv
sleep 32

for (( i = 0; i < 10; i++ )); do
    clang-build/bench/bin/bench --benchmark_format=csv --benchmark_enable_random_interleaving=true | tail --lines=+2 | tee -a epath-bench/analysis/config_03.csv
    sleep 32
done

fcsv epath-bench/analysis/config_02.csv | sponge epath-bench/analysis/config_02.csv
fcsv epath-bench/analysis/config_03.csv | sponge epath-bench/analysis/config_03.csv
