@echo off

build\bench\bin\bench --benchmark_format=csv --benchmark_enable_random_interleaving=true > epath-bench\analysis\config_06.csv
timeout 32 > NUL

for /l %%i in (1, 1, 10) do (
   build\bench\bin\bench --benchmark_format=csv --benchmark_enable_random_interleaving=true | more +1 >> epath-bench\analysis\config_06.csv
   timeout 32 > NUL
)
