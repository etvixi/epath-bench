# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                                  IMPORT                                   │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from pathlib import Path
from collections import defaultdict
from tabulate import tabulate

plt.style.use('one_dark')
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
fontfamily = 'Iosevka SS05'

# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                               DEFINITIONS                                 │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘


class row_data(object):
    """Useful data from a row in csv file."""

    def __init__(self, csvrow, realtime_index: int):
        super(row_data, self).__init__()
        benchname, lengthstr, insertstr = csvrow[0].split('/')
        self.benchname = benchname
        self.length = int(lengthstr)
        self.inserts = int(insertstr)
        self.real_time = float(csvrow[realtime_index])


class measured(object):
    """Accumulator for measurements."""

    def __init__(self):
        super(measured, self).__init__()
        self.points = defaultdict(list)
        self.lengths = set()
        self.inserts = set()

    def __call__(self, row: row_data):
        self.points[(row.length, row.inserts)].append(row.real_time)
        self.lengths.add(row.length)
        self.inserts.add(row.inserts)


class loaded_masurements(object):
    """All measurements loaded from a CSV-file."""

    def __init__(self, datapath: str):
        super(loaded_masurements, self).__init__()
        self.benchmarks = defaultdict(measured)
        with open(datapath) as csvfile:
            reader = csv.reader(csvfile)
            csvheader = [name.strip() for name in next(reader)]
            realtime_index = csvheader.index('real_time')
            for row in reader:
                datarow = row_data(
                    [entry.strip() for entry in row], realtime_index)
                self.benchmarks[datarow.benchname](datarow)


class benchmark_data(object):
    """Measurements per benchmark."""

    def __init__(self, data: measured):
        super(benchmark_data, self).__init__()
        self.features = list()
        self.times = list()
        for (length, inserts), times in data.points.items():
            feature = [float(length), float(inserts)]
            self.features += len(times) * [feature]
            self.times += times


class median_benchmark_data(object):
    """Measurements per benchmark."""

    def __init__(self, data: measured):
        super(median_benchmark_data, self).__init__()
        self.features = list()
        self.times = list()
        for (length, inserts), times in data.points.items():
            self.features.append([float(length), float(inserts)])
            self.times.append(np.median(times))


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                             LEAST SQUARES FIT                             │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

class normalized_features(object):
    """Normalzie features for numeric stability."""

    def __init__(self, data: benchmark_data):
        super(normalized_features, self).__init__()
        X, Y = np.asarray(data.features).T
        Z = np.asarray(data.times)

        mask = X > (2 * Y)
        X = X[mask]
        Y = Y[mask]
        Z = Z[mask]

        self.bias = [np.median(X), np.median(Y), np.median(Z)]
        X = X - self.bias[0]
        Y = Y - self.bias[1]
        Z = Z - self.bias[2]

        self.spread = [
            np.median(np.abs(X)), np.median(np.abs(Y)), np.median(np.abs(Z))]
        self.x = X / self.spread[0]
        self.y = Y / self.spread[1]
        self.z = Z / self.spread[2]

        self.count = len(self.x)

    def find_error(self, M: np.ndarray, A: np.ndarray):
        """ Compare predictions with fitted results:
            M - matrix of measurements of basis functions
            A - fitted model coefficients
        """
        d = np.dot(M, A) - self.z
        return np.sqrt(d.dot(d) / self.count) * self.spread[2] / self.bias[2]


class harmonic_model(normalized_features):
    """Least squares fit by"""
    """a11 * x * y + a10 * x + a01 * y + a00 polynomial."""

    def __init__(self, data: benchmark_data):
        super(harmonic_model, self).__init__(data)
        MT = np.row_stack(
            (self.x * self.y, self.x, self.y, np.ones((self.count))))
        M = MT.T
        A = np.dot(np.dot(np.linalg.pinv(np.dot(MT, M)), MT), self.z)

        self.R = self.find_error(M, A)
        A = np.asarray([
            A[0] * self.spread[2] / (self.spread[0] * self.spread[1]),
            A[1] * self.spread[2] / self.spread[0],
            A[2] * self.spread[2] / self.spread[1],
            A[3] * self.spread[2] + self.bias[2]])
        self.a = np.asarray([
            A[0],
            A[1] - A[0] * self.bias[1],
            A[2] - A[0] * self.bias[0],
            A[3] - A[1] * self.bias[0] - A[2] * self.bias[1]
            + A[0] * self.bias[0] * self.bias[1]])

    def __call__(self, length, inserts):
        return self.a[0] * length * inserts \
            + self.a[1] * length + self.a[2] * inserts + self.a[3]


class lin_model(normalized_features):
    """Least squares fit by"""
    """a10 * x + a01 * y + a00 polynomial."""

    def __init__(self, data: benchmark_data):
        super(lin_model, self).__init__(data)

        MT = np.row_stack((self.x, self.y, np.ones((self.count))))
        M = MT.T
        A = np.dot(np.dot(np.linalg.pinv(np.dot(MT, M)), MT), self.z)

        self.R = self.find_error(M, A)

        self.a = np.asarray([
            A[0] * self.spread[2] / self.spread[0],
            A[1] * self.spread[2] / self.spread[1],
            A[2] * self.spread[2] + self.bias[2]
        ])
        self.a[2] = \
            self.a[2] - self.a[0] * self.bias[0] - self.a[1] * self.bias[1]

    def __call__(self, length, inserts):
        return self.a[0] * length + self.a[1] * inserts + self.a[2]


class x_model(normalized_features):
    """Least squares fit by a0 * x + a1 polynomial."""

    def __init__(self, data: benchmark_data):
        super(x_model, self).__init__(data)

        MT = np.row_stack((self.x, np.ones((self.count))))
        M = MT.T
        A = np.dot(np.dot(np.linalg.pinv(np.dot(MT, M)), MT), self.z)

        self.R = self.find_error(M, A)

        self.a = np.asarray([
            A[0] * self.spread[2] / self.spread[0],
            A[1] * self.spread[2] + self.bias[2]
        ])
        self.a[1] = self.a[1] - self.a[0] * self.bias[0]

    def __call__(self, length, inserts):
        return self.a[0] * length + self.a[1]


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                                   PLOTS                                   │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

def plot_time_vs_inserts(
        loaded: loaded_masurements,
        benchname: str,
        length: int,
        axis):
    points = loaded.benchmarks[benchname].points
    inserts = sorted(list(loaded.benchmarks[benchname].inserts))
    x0, y0 = np.asarray(
        [[float(ins), time]
         for ins in inserts
         for time in points[(length, ins)]]).T

    medians = np.asarray(
        [[float(ins), np.median(points[(length, ins)])]
         for ins in inserts])

    x1, y1 = medians[medians[:, 0].argsort()].T

    axis.scatter(x0, y0, s=6, color=colors[1])
    axis.plot(x1, y1, color=colors[0])
    axis.scatter(x1, y1, s=24, color=colors[0])

    font = mpl.font_manager.FontProperties(family=fontfamily)
    for l in axis.get_xticklabels():
        l.set_fontproperties(font)
    for l in axis.get_yticklabels():
        l.set_fontproperties(font)


def plot_time_vs_length(
        loaded: loaded_masurements,
        benchname: str,
        inserts: int,
        axis):
    points = loaded.benchmarks[benchname].points
    lengths = sorted(list(loaded.benchmarks[benchname].lengths))
    x0, y0 = np.asarray(
        [[float(length), time]
         for length in lengths
         for time in points[(length, inserts)]]).T

    medians = np.asarray(
        [[float(length), np.median(points[(length, inserts)])]
         for length in lengths])

    x1, y1 = medians[medians[:, 0].argsort()].T

    axis.scatter(x0, y0, s=6, color=colors[1])
    axis.plot(x1, y1, color=colors[0])
    axis.scatter(x1, y1, s=24, color=colors[0])

    font = mpl.font_manager.FontProperties(family=fontfamily)
    for l in axis.get_xticklabels():
        l.set_fontproperties(font)
    for l in axis.get_yticklabels():
        l.set_fontproperties(font)


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                                 READ DATA                                 │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

thisdir = '/home/grisha/source/playground/epath/epath-bench/analysis'
datapath = f'{thisdir}/config_04.csv'
configname = Path(datapath).stem
loaded = loaded_masurements(datapath)


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                           PLOT TIME VS INSERTS                            │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

# [13, 19, 29, 71, 173]
length = 13

for benchname in loaded.benchmarks.keys():
    fig, ax = plt.subplots()
    ax.set_title(f'{benchname}, {configname}, length = {length}',
                 fontfamily=fontfamily)
    ax.set_xlabel('inserts', fontfamily=fontfamily)
    ax.set_ylabel('time [ns]', fontfamily=fontfamily)
    ax.grid(True)
    ax.set_axisbelow(True)

    plot_time_vs_inserts(loaded, benchname, length, ax)
    plt.savefig(f'{thisdir}/{configname}/L{length}_{benchname}.png')
    plt.close()


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                            PLOT TIME VS LENGTH                            │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

# inserts = [1, 2, 4, 6, 8]
inserts = 4

for benchname in loaded.benchmarks.keys():
    fig, ax = plt.subplots()
    ax.set_title(f'{benchname}, {configname}, inserts = {inserts}',
                 fontfamily=fontfamily)
    ax.set_xlabel('length', fontfamily=fontfamily)
    ax.set_ylabel('time [ns]', fontfamily=fontfamily)
    ax.grid(True)
    ax.set_axisbelow(True)

    plot_time_vs_length(loaded, benchname, inserts, ax)
    plt.savefig(f'{thisdir}/{configname}/I{inserts}_{benchname}.png')
    plt.close()


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                                  X-MODEL                                  │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

fits = {key: x_model(benchmark_data(value))
        for key, value in loaded.benchmarks.items()}

result_rows = [[key, model.R] + list(model.a)
               for key, model in fits.items()]

print(tabulate(
    result_rows,
    headers=["Benchmark", "R", "time/char", "delay"],
    tablefmt="github"))


norm = fits['EscapePath'].a[0]
result_rows = [[key, model.R, model.a[0] / norm]
               for key, model in fits.items()]

print(tabulate(
    result_rows,
    headers=["Benchmark", "R", "time/char"],
    tablefmt="github"))

# | Benchmark               |         R |   time/char |     delay |
# |-------------------------|-----------|-------------|-----------|
# | ReferenceEscapePath     | 0.0453066 |    15.1729  |  -34.7004 |
# | EscapePathRegexPrealloc | 0.100956  |   145.268   | -625.33   |
# | EscapePathBoost         | 0.0463609 |    15.5225  |   70.3014 |
# | EscapePathRegex         | 0.124231  |   149.371   | -639.37   |
# | EscapePath              | 0.046145  |     3.52764 |   27.2495 |
#
# | Benchmark               |         R |   time/char |
# |-------------------------|-----------|-------------|
# | ReferenceEscapePath     | 0.0453066 |     4.30115 |
# | EscapePathRegexPrealloc | 0.100956  |    41.18    |
# | EscapePathBoost         | 0.0463609 |     4.40026 |
# | EscapePathRegex         | 0.124231  |    42.3431  |
# | EscapePath              | 0.046145  |     1       |


# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                               MANUAL R-TEST                               │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

errors = list()
for benchname, measurement in loaded.benchmarks.items():
    fit = fits[benchname]
    bench = benchmark_data(measurement)

    count = 0
    error = 0.

    for t, (l, i) in zip(bench.times, bench.features):
        count = count + 1
        delta = fit(l, i) - t
        error += (delta * delta - error) / count

    errors.append([benchname, np.sqrt(error) / np.median(bench.times)])

print(tabulate(
    errors,
    headers=["Benchmark", "R"],
    tablefmt="github"))

# %%
# ┌───────────────────────────────────────────────────────────────────────────┐
# │                                                                           │
# │                              MEDIAN X-MODEL                               │
# │                                                                           │
# └───────────────────────────────────────────────────────────────────────────┘

fits = {key: x_model(median_benchmark_data(value))
        for key, value in loaded.benchmarks.items()}

result_rows = [[key, model.R] + list(model.a)
               for key, model in fits.items()]

print(tabulate(
    result_rows,
    headers=["Benchmark", "R", "time/char", "delay"],
    tablefmt="github"))


norm = fits['EscapePath'].a[0]
result_rows = [[key, model.R, model.a[0] / norm]
               for key, model in fits.items()]

print(tabulate(
    result_rows,
    headers=["Benchmark", "R", "time/char"],
    tablefmt="github"))
