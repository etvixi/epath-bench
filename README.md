# Replace `%` with `%25` in a string

These exercises are inspired ["For processing strings, streams in C++ can be slow"](https://lemire.me/blog/2023/10/19/for-processing-strings-streams-in-c-can-be-slow/) blog-post by Daniel Lemire.

There are five benchmarks of the functions from `/lib/epath/` directory:

| Benchmark               | Explanation                                                             |
| ----------------------- | ----------------------------------------------------------------------- |
| EscapePath              | no string specific algorithms                                           |
| ReferenceEscapePath     | reference algorithm from the blogpost                                   |
| EscapePathRegex         | `std::regex_replace(std::string, std::regex, char const *)`             |
| EscapePathRegexPrealloc | `std::regex_replace(OutIter, BiIter, BiIter, std::regex, char const *)` |
| EscapePathBoost         | `boost::algorithm::replace_all`                                         |

## Results

| Machine | CPU                             | L1 Data      | L1 Instruction | L2 Unified     | L3 Unified     |
| ------- | ------------------------------- | ------------ | -------------- | -------------- | -------------- |
| 1       | Intel i7-12700H (20) @ 4.600GHz | 48 KiB (x10) | 32 KiB (x10)   | 1280 KiB (x10) | 24576 KiB (x1) |
| 2       | Intel i7-3740QM (8) @ 3.700GHz  | 32 KiB (x4)  | 32 KiB (x4)    | 256 KiB (x4)   | 6144 KiB (x1)  |
| 3       | Intel i7-8086K (12) @ 5.000GHz  | 32 KiB (x6)  | 32 KiB (x6)    | 256 KiB (x6)   | 12288 KiB (x1) |

| Name      | Machine | OS                    | Compiler         |
| --------- | ------- | --------------------- | ---------------- |
| config_00 | 1       | Ubuntu 22.04.3        | GCC 11.4.0       |
| config_01 | 1       | Ubuntu 22.04.3        | Clang 17.0       |
| config_02 | 2       | Ubuntu 20.04.6        | GCC 10.5.0       |
| config_03 | 2       | Ubuntu 20.04.6        | Clang 10.0.1     |
| config_04 | 3       | Ubuntu 22.04.3        | GCC 12.3.0       |
| config_05 | 3       | Ubuntu 22.04.3        | Clang 14.0.0     |
| config_06 | 2       | MS Windows 10.0.19045 | MSVC 19.29.30153 |

| config_00               | R          | time/char | relative time/char |
| ----------------------- | ---------- | --------- | ------------------ |
| EscapePath              | 0.0479593  | 1.45569   | 1                  |
| ReferenceEscapePath     | 0.118105   | 6.63719   | 4.55949            |
| EscapePathBoost         | 0.0673391  | 5.77401   | 3.96652            |
| EscapePathRegex         | 0.0218326  | 58.7586   | 40.3648            |
| EscapePathRegexPrealloc | 0.00982875 | 59.5176   | 40.8862            |

| config_01               | R          | time/char | relative time/char |
| ----------------------- | ---------- | --------- | ------------------ |
| EscapePath              | 0.0548299  | 1.09434   | 1                  |
| ReferenceEscapePath     | 0.116352   | 6.70445   | 6.12646            |
| EscapePathBoost         | 0.0607646  | 7.89655   | 7.21579            |
| EscapePathRegex         | 0.0167916  | 64.3238   | 58.7786            |
| EscapePathRegexPrealloc | 0.00981574 | 63.619    | 58.1344            |

| config_02               | R         | time/char | relative time/char |
| ----------------------- | --------- | --------- | ------------------ |
| EscapePath              | 0.0258173 | 3.53556   | 1                  |
| ReferenceEscapePath     | 0.0380419 | 15.2014   | 4.29957            |
| EscapePathBoost         | 0.0413702 | 15.4925   | 4.38189            |
| EscapePathRegex         | 0.0128387 | 146.974   | 41.5703            |
| EscapePathRegexPrealloc | 0.0104425 | 143.987   | 40.7253            |

| config_03               | R         | time/char | relative time/char |
| ----------------------- | --------- | --------- | ------------------ |
| EscapePath              | 0.0127091 | 2.47879   | 1                  |
| ReferenceEscapePath     | 0.040594  | 14.9583   | 6.03451            |
| EscapePathBoost         | 0.0250749 | 18.751    | 7.56455            |
| EscapePathRegex         | 0.0168495 | 151.906   | 61.2824            |
| EscapePathRegexPrealloc | 0.0044148 | 152.188   | 61.3961            |

| config_04               | R         | time/char | relative time/char |
| ----------------------- | --------- | --------- | ------------------ |
| EscapePath              | 0.0228467 | 2.40972   | 1                  |
| ReferenceEscapePath     | 0.0427068 | 11.7001   | 4.85535            |
| EscapePathBoost         | 0.0309049 | 13.2675   | 5.50583            |
| EscapePathRegex         | 0.0248336 | 85.8896   | 35.6429            |
| EscapePathRegexPrealloc | 0.0156545 | 84.5117   | 35.0711            |

| config_05               | R         | time/char | relative time/char |
| ----------------------- | --------- | --------- | ------------------ |
| EscapePath              | 0.0294739 | 2.12081   | 1                  |
| ReferenceEscapePath     | 0.0492383 | 11.9641   | 5.64131            |
| EscapePathRegex         | 0.0188336 | 91.9486   | 43.3555            |
| EscapePathBoost         | 0.0361376 | 15.4689   | 7.29386            |
| EscapePathRegexPrealloc | 0.0078280 | 91.2873   | 43.0437            |

| config_06               | R         | time/char | relative time/char |
| ----------------------- | --------- | --------- | ------------------ |
| EscapePath              | 0.0533075 | 9.50001   | 1                  |
| ReferenceEscapePath     | 0.130615  | 21.8261   | 2.29749            |
| EscapePathBoost         | 0.0364985 | 42.2131   | 4.44348            |
| EscapePathRegex         | 0.040294  | 623.842   | 65.6675            |
| EscapePathRegexPrealloc | 0.03261   | 656.201   | 69.0738            |

## How to build

You need [Conan-1.XX](https://docs.conan.io/en/1.62/index.html) package manager to manage dependencies.

```bash
mkdir epath && cd epath
git clone https://gitlab.com/etvixi/epath-bench.git
mkdir build
conan install epath-bench/ -if build/ --build missing
cmake -S epath-bench/ -B build -DCMAKE_BUILD_TYPE=Release
cmake --build build
```

Now, you can launch tests:

```bash
build/test/bin/test
```

or benchmarks:

```bash
build/bench/bin/bench
```

## License

All code except the original code in `lib/epath/original.hpp` are distributed under Apache NON-AI License Version 2.0.

> Apache NON-AI License, Version 2.0
>
> TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
>
> 1.  Definitions.
>
> “License” shall mean the terms and conditions for use, reproduction, and distribution as defined by Sections 1 through 9 of this document.
>
> “Licensor” shall mean the copyright owner or entity authorized by the copyright owner that is granting the License.
>
> “Legal Entity” shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, “control” means > (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) > beneficial ownership of such entity.
>
> “You” (or “Your”) shall mean an individual or Legal Entity exercising permissions granted by this License.
>
> “Source” form shall mean the preferred form for making modifications, including but not limited to software source code, documentation source, and configuration files.
>
> “Object” form shall mean any form resulting from mechanical transformation or translation of a Source form, including but not limited to compiled object code, generated documentation, and conversions to other > media types.
>
> “Work” shall mean the work of authorship, whether in Source or Object form, made available under the License, as indicated by a copyright notice that is included in or attached to the work (an example is provided > in the Appendix below).
>
> “Derivative Works” shall mean any work, whether in Source or Object form, that is based on (or derived from) the Work and for which the editorial revisions, annotations, elaborations, or other modifications > represent, as a whole, an original work of authorship. For the purposes of this License, Derivative Works shall not include works that remain separable from, or merely link (or bind by name) to the interfaces of, > the Work and Derivative Works thereof.
>
> “Contribution” shall mean any work of authorship, including the original version of the Work and any modifications or additions to that Work or Derivative Works thereof, that is intentionally submitted to > Licensor for inclusion in the Work by the copyright owner or by an individual or Legal Entity authorized to submit on behalf of the copyright owner. For the purposes of this definition, “submitted” means any form > of electronic, verbal, or written communication sent to the Licensor or its representatives, including but not limited to communication on electronic mailing lists, source code control systems, and issue tracking > systems that are managed by, or on behalf of, the Licensor for the purpose of discussing and improving the Work, but excluding communication that is conspicuously marked or otherwise designated in writing by the > copyright owner as “Not a Contribution.”
>
> “Contributor” shall mean Licensor and any individual or Legal Entity on behalf of whom a Contribution has been received by Licensor and subsequently incorporated within the Work.
>
> 2.  Grant of Copyright License.
>
> Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable copyright license to reproduce, prepare > Derivative Works of, publicly display, publicly perform, sublicense, and distribute the Work and such Derivative Works in Source or Object form, under the following conditions:
>
>   2.1. You shall not use the Covered Software in the creation of an Artificial Intelligence training dataset, including but not limited to any use that contributes to the training or development of an AI model or > algorithm, unless You obtain explicit written permission from the Contributor to do so.
>
>   2.2. You acknowledge that the Covered Software is not intended for use in the creation of an Artificial Intelligence training dataset, and that the Contributor has no obligation to provide support or assistance > for any use that violates this license.
>
> 3.  Grant of Patent License.
>
> Subject to the terms and conditions of this License, each Contributor hereby grants to You a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent > license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work, where such license applies only to those patent claims licensable by such Contributor that are necessarily infringed > by their Contribution(s) alone or by combination of their Contribution(s) with the Work to which such Contribution(s) was submitted. If You institute patent litigation against any entity (including a cross-claim > or counterclaim in a lawsuit) alleging that the Work or a Contribution incorporated within the Work constitutes direct or contributory patent infringement, then any patent licenses granted to You under this > License for that Work shall terminate as of the date such litigation is filed.
>
> 4.  Redistribution.
>
> You may reproduce and distribute copies of the Work or Derivative Works thereof in any medium, with or without modifications, and in Source or Object form, provided that You meet the following conditions:
>
> 1.  You must give any other recipients of the Work or Derivative Works a copy of this License; and
>
> 2.  You must cause any modified files to carry prominent notices stating that You changed the files; and
>
> 3.  You must retain, in the Source form of any Derivative Works that You distribute, all copyright, patent, trademark, and attribution notices from the Source form of the Work, excluding those notices that do > not pertain to any part of the Derivative Works; and
>
> 4.  If the Work includes a “NOTICE” text file as part of its distribution, then any Derivative Works that You distribute must include a readable copy of the attribution notices contained within such NOTICE file, > excluding those notices that do not pertain to any part of the Derivative Works, in at least one of the following places: within a NOTICE text file distributed as part of the Derivative Works; within the Source > form or documentation, if provided along with the Derivative Works; or, within a display generated by the Derivative Works, if and wherever such third-party notices normally appear. The contents of the NOTICE > file are for informational purposes only and do not modify the License.
>
> You may add Your own attribution notices within Derivative Works that You distribute, alongside or as an addendum to the NOTICE text from the Work, provided that such additional attribution notices cannot be > construed as modifying the License.
>
> You may add Your own copyright statement to Your modifications and may provide additional or different license terms and conditions for use, reproduction, or distribution of Your modifications, or for any such > Derivative Works as a whole, provided Your use, reproduction, and distribution of the Work otherwise complies with the conditions stated in this License.
>
> 5.  Submission of Contributions.
>
> Unless You explicitly state otherwise, any Contribution intentionally submitted for inclusion in the Work by You to the Licensor shall be under the terms and conditions of this License, without any additional > terms or conditions. Notwithstanding the above, nothing herein shall supersede or modify the terms of any separate license agreement you may have executed with Licensor regarding such Contributions.
>
> 6.  Trademarks.
>
> This License does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Work > and reproducing the content of the NOTICE file.
>
> 7.  Disclaimer of Warranty.
>
> Unless required by applicable law or agreed to in writing, Licensor provides the Work (and each Contributor provides its Contributions) on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either > express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the > appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.
>
> 8.  Limitation of Liability.
>
> In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, > shall any Contributor be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this License or out of the use or > inability to use the Work (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if such Contributor > has been advised of the possibility of such damages.
>
> 9.  Accepting Warranty or Additional Liability.
>
> While redistributing the Work or Derivative Works thereof, You may choose to offer, and charge a fee for, acceptance of support, warranty, indemnity, or other liability obligations and/or rights consistent with > this License. However, in accepting such obligations, You may act only on Your own behalf and on Your sole responsibility, not on behalf of any other Contributor, and only if You agree to indemnify, defend, and > hold each Contributor harmless for any liability incurred by, or claims asserted against, such Contributor by reason of your accepting any such warranty or additional liability.
>
> END OF TERMS AND CONDITIONS
