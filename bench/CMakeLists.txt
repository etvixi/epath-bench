# Copyright 2023 Grigory Kirilin
#
# Licensed under the Apache NON-AI License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://github.com/non-ai-licenses/non-ai-licenses/blob/main/NON-AI-APACHE2
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cmake_minimum_required(VERSION 3.10)

project(bench)
if (NOT TARGET epath)
    add_subdirectory(../lib ${CMAKE_BINARY_DIR}/epath)
endif()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_executable(
    bench
        src/bench_main.cpp
        src/benchmark_escape_path.cpp
)

target_link_libraries(
    bench
    PRIVATE
        ${CONAN_LIBS}
        epath
)
