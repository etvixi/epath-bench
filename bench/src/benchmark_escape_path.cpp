// Copyright 2023 Grigory Kirilin
//
// Licensed under the Apache NON-AI License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://github.com/non-ai-licenses/non-ai-licenses/blob/main/NON-AI-APACHE2
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <benchmark/benchmark.h>

#include "epath/epath.hpp"
#include "epath/original.hpp"

#include <thread>
#include <random>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;

auto const seed    = std::random_device{} ();
auto const lengths = std::vector<int64_t>{13, 19, 29, 71, 173};
auto const inserts = std::vector<int64_t>{1, 2, 4, 6, 8};

/**
 * @details Cool down CPU 10 seconds before next benchmark.
 */
std::string make_input(int64_t length, int64_t count)
{
  std::this_thread::sleep_for(1s);
  auto rne = std::default_random_engine{seed};
  char const symbols[] = "abcABC";

  auto result = std::string(length, '%');
  std::sample(
    std::cbegin(symbols), std::cend(symbols),
    result.begin(), length - count, rne);
  std::shuffle(result.begin(), result.end(), rne);
  return result;
}

static void EscapePath(benchmark::State &state)
{
  auto input = make_input(state.range(0), state.range(1));
  for (auto _ : state) {
    std::string path = escape_path(input);
    benchmark::DoNotOptimize(path);
  }
}

BENCHMARK(EscapePath)->ArgsProduct({lengths, inserts});

static void ReferenceEscapePath(benchmark::State &state)
{
  auto input = make_input(state.range(0), state.range(1));
  for (auto _ : state) {
    std::string path = find_string_escape(input);
    benchmark::DoNotOptimize(path);
  }
}

BENCHMARK(ReferenceEscapePath)->ArgsProduct({lengths, inserts});

static void EscapePathRegexPrealloc(benchmark::State &state)
{
  auto input = make_input(state.range(0), state.range(1));
  for (auto _ : state) {
    std::string path = escape_path_regex_prealloc(input);
    benchmark::DoNotOptimize(path);
  }
}

BENCHMARK(EscapePathRegexPrealloc)->ArgsProduct({lengths, inserts});

static void EscapePathRegex(benchmark::State &state)
{
  auto input = make_input(state.range(0), state.range(1));
  for (auto _ : state) {
    std::string path = escape_path_regex(input);
    benchmark::DoNotOptimize(path);
  }
}

BENCHMARK(EscapePathRegex)->ArgsProduct({lengths, inserts});


static void EscapePathBoost(benchmark::State &state)
{
  auto input = make_input(state.range(0), state.range(1));
  for (auto _ : state) {
    std::string path = escape_path_boost(input);
    benchmark::DoNotOptimize(path);
  }
}

BENCHMARK(EscapePathBoost)->ArgsProduct({lengths, inserts});
